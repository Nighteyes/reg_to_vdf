# reg_to_vdf

Converts a reg file to a registry.vdf file. This is for use with the following steam launcher kodi plugin:<br>
https://forum.kodi.tv/showthread.php?tid=326662<br>
Please do note that this is just a quick hack made as fast as possible :D.<br>
<PRE>
Usage: reg_to_vdf.exe [-i targetfile] [-e exportfile]

Options:
     -i          Import a registry file, which should be an export the
                 following key:
                 "HKEY_CURRENT_USER\Software\Valve\Steam\Apps\".
     -e          Export a vdf file, optional and defaults to registry.vdf.
<br>
Example usage:   reg_to_vdf.exe -i "D:\importdir\import.reg"
                 reg_to_vdf.exe -i D:\importdir\import.reg 
                 reg_to_vdf.exe -i import.reg