﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reg_to_vdf
{
    class Program
    {
        static int _depth = 0;
        static StringBuilder _outputBuilder;
        static string[] registryLines;

        static void Main(string[] args)
        {

            string import = null;
            string export = null;

            if (args == null || args.Length == 0)
            {
                ShowUsage();
                return;
            }

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Contains("-h") || args[i].Contains("--help"))
                {
                    ShowUsage();
                    return;
                }

                if (args[i].Contains("-i"))
                    import = args[i + 1].Trim('"');

                if (args[i].Contains("-e"))
                    export = args[i + 1].Trim('"');
            }

            if (import == null)
            {
                Console.WriteLine("No import file specified.");
                return;
            }     
            
            if(!import.Contains("\\"))
            {
                import = AppDomain.CurrentDomain.BaseDirectory + import;
            } 

            try
            {
                Console.WriteLine("Importing "+import);
                registryLines = System.IO.File.ReadAllLines(import);
            }
            catch
            {
                Console.WriteLine("File not found.");
                return;
            }

            _outputBuilder = new StringBuilder();

            Console.WriteLine("Converting " + import);
            CreateOutput();


            if (export == null)
            {
                export = AppDomain.CurrentDomain.BaseDirectory+"registry.vdf";
            }

            try
            {
                Console.WriteLine("Exporting to " + export);                
                System.IO.File.WriteAllText(export, _outputBuilder.ToString());
            }
            catch
            {
                Console.WriteLine("Could not write to file.");
            }
        }

        static void ShowUsage()
        {
            Console.WriteLine("");
            Console.WriteLine("Usage: reg_to_vdf.exe [-i targetfile] [-e exportfile]");
            Console.WriteLine("");
            Console.WriteLine("Options: ");
            Console.WriteLine("     -i          Import a registry file, which should be an export the");
            Console.WriteLine("                 following key: ");
            Console.WriteLine("                 \"HKEY_CURRENT_USER\\Software\\Valve\\Steam\\Apps\".");
            Console.WriteLine("     -e          Export a vdf file, optional and defaults to registry.vdf.");
            Console.WriteLine("");
            Console.WriteLine("Example usage:   reg_to_vdf.exe -i \"D:\\importdir\\import.reg\"");
            Console.WriteLine("Example usage:   reg_to_vdf.exe -i D:\\importdir\\import.reg");            
        }

        static void CreateOutput()
        {
            _outputBuilder.Append("\"Registry\""); Enter();
            _outputBuilder.Append("{"); _depth++; Enter();
            _outputBuilder.Append("\"HKCU\""); Enter();
            _outputBuilder.Append("{"); _depth++; Enter();
            _outputBuilder.Append("\"Software\""); Enter();
            _outputBuilder.Append("{"); _depth++; Enter();
            _outputBuilder.Append("\"Valve\""); Enter();
            _outputBuilder.Append("{"); _depth++; Enter();
            _outputBuilder.Append("\"Steam\""); Enter();
            _outputBuilder.Append("{"); _depth++; Enter();

            _outputBuilder.Append("\"Apps\""); Enter();
            _outputBuilder.Append("{"); _depth++;

            AddApps();

            _depth--; Enter(); _outputBuilder.Append("}");
            _depth--; Enter(); _outputBuilder.Append("}");
            _depth--; Enter(); _outputBuilder.Append("}");
            _depth--; Enter(); _outputBuilder.Append("}");
            _depth--; Enter(); _outputBuilder.Append("}");
            _depth--; Enter(); _outputBuilder.Append("}");
        }

        static void AddApps()
        {

            string[] split = null;
            string[] splitValues = null;
            bool foundInstalledFlag = false;

            for (int i = 0; i < registryLines.Length; i++)
            {

                split = null;
                split = registryLines[i].Split('\\');

                if (split == null)
                    continue;
                foundInstalledFlag = false;
                if (split.Length > 2 && split[split.Length - 2].Contains("Apps"))
                {

                    Enter();

                    _outputBuilder.Append("\"" + split[split.Length - 1].Trim(']') + "\""); Enter();
                    _outputBuilder.Append("{"); _depth++; Enter();

                    int step = i + 1;

                    while (step <= registryLines.Length - 1 && !registryLines[step].Contains("HKEY_CURRENT_USER"))
                    {
                        splitValues = registryLines[step].Split('=');

                        if (splitValues != null && splitValues.Length > 1 && splitValues[0].Contains("Installed"))
                        {
                            foundInstalledFlag = true;
                            _outputBuilder.Append("\"installed\""); Tab(2); _outputBuilder.Append("\"" + splitValues[1][splitValues[1].Length - 1] + "\"");
                            break;
                        }

                        splitValues = null;
                        step++;

                    }

                    if (!foundInstalledFlag)
                    {
                        _outputBuilder.Append("\"installed\""); Tab(2); _outputBuilder.Append("\"0\"");
                    }

                    _depth--; Enter();
                    _outputBuilder.Append("}");
                }

            }
        }

        static void Tab(int amount = 0)
        {
            if (amount == 0)
                amount = _depth;

            for (int i = 0; i < amount; i++)
            {
                _outputBuilder.Append('\t');
            }
        }

        static void Enter(int amount = 1)
        {
            for (int i = 0; i < amount; i++)
            {
                _outputBuilder.Append('\n');
            }

            Tab();
        }
    }
}
